
const mongoose = require('mongoose');
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
  GraphQLID
} = require('graphql');

// take in the model from the mongoose schema to use in the query and mutations.
const userModel = require('../models/users');

const UserType = new GraphQLObjectType({
  name: 'User',
  description: 'The model for a single user. The id is supplied by the database. The updated field is (for now) just the current date stamp',
  fields: () => ({
    _id: { type: GraphQLID },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    occupation: { type: GraphQLString },
    updated: { type: GraphQLString }
  })
});

// mutations in graphql are essentially the C, U, and D of a CRUD app.
const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addUser: {
      type: UserType,
      description: 'Creates a new user entry in the database',
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
        occupation: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve(parentVal, args) {
        const newUser = new Promise((resolve, reject) => {
          const user = new userModel({
            name: args.name,
            age: args.age,
            occupation: args.occupation
          }).save((err, user) => {
            if (err) {
              reject(err);
            }
            resolve(user);
          })
        });
        return newUser;
      }
    },
    updateUser: {
      type: UserType,
      description: 'Updates a single user by their id',
      args: {
        _id: { type: new GraphQLNonNull(GraphQLID) },
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        occupation: { type: GraphQLString }
      },
      resolve(parentVal, args) {
        const updatedUser = new Promise((resolve, reject) => {
          userModel.findById(args._id, (err, user) => {
            if (err) {
              reject(err);
            }
            if (args.name) {
              user.name = args.name;
            }
            if (args.age) {
              user.age = args.age;
            }
            if (args.occupation) {
              user.occupation = args.occupation
            }

            user.save((err) => {
              if (err) {
                reject(err);
              }
              resolve(user);
            })
          })
        })
        return updatedUser;
      }
    },
    deleteUser: {
      type: UserType,
      description: 'Deletes a single user by their id',
      args: {
        _id: { type: GraphQLID }
      },
      resolve(parentVal, args) {
        const deletedUser = new Promise((resolve, reject) => {
          userModel.remove({ _id: args._id }, (err, user) => {
            if (err) {
              reject(err);
            }
            resolve(user);
          })
        })
        return deletedUser
      }
    }
  }
})

const RootQuery = new GraphQLObjectType({
  // the name is what shows up in the graphiql documentation section.
  name: 'RootQuery',
  description: 'This query will return either one user or all the users',
  // fields are what show up in the graphiql docs as well.
  fields: {
    // the "user" is the thing that will be queried.
    user: {
      // the "type" refers to which GraphQLObjectType is being queried
      type: UserType,
      description: 'Returns a single user by id (supplied by the db)',
      // the args are what the query will take in to find the individual user in the db.
      args: {
        _id: { type: GraphQLID }
      },
      // the resolver function contains the db operation. it needs to return a user.
      resolve(parentVal, args) {
        const foundUser = new Promise((resolve, reject) => {
          userModel.findById(args._id, (err, user) => {
            if (err) {
              reject(err)
            }
            resolve(user);
          })
        })
        return foundUser;
      }
    },
    allUsers: {
      // here we create a new GraphQLList which takes the UserType as an argument.
      // then we `find` all the users and return them.
      type: new GraphQLList(UserType),
      description: 'Returns a list of all the users',
      resolve() {
        const foundAllUsers = new Promise((resolve, reject) => {
          userModel.find((err, users) => {
            if (err) {
              reject(users);
            }
            resolve(users);
          })
        })
        return foundAllUsers;
      }
    }
  }
});

// the GraphQLSchema bundles together the query and the mutations
const userSchema = new GraphQLSchema({
  query: RootQuery,
  mutation
});

module.exports = userSchema;