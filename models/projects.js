const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProjectModelSchema = new Schema({
  title: String,
  owner: {
    type: Schema.ObjectId,
    ref: 'UserModel'
  },
  updated: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('ProjectModel', ProjectModelSchema);