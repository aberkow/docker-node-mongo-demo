const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserModelSchema = new Schema({
  name: String,
  age: { 
    type: Number, 
    min: [0, 'uhhh no...'], 
    max: [120, 'doubt it...'] 
  },
  occupation: {
    type: String,
    lowercase: true
  },
  updated: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('UserModel', UserModelSchema);