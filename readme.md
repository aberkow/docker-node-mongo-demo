# MongoDB/GraphQL app

This is an example of a ME(?)N app. It's still a work in progress, but here's what's going on so far.

## Background
I wanted to see if I could build a dockerized app using express and mongo/mongoose. In the end, I've decided to use graphql to handle the API instead of REST.

## Usage
`$ docker-compose up`
The server will be created before the database, but it will only begin listening after mongodb is connected.

This app mostly uses the graphiql interface to examine the queries. To see it, start the app and visit localhost:3000/graphql

## Structure

### server.js
Creates 
- a connection to the mongodb database
- an express server

### /graphql
Conains the graphql schemas to be imported into the app. The fields of the schemas should closely match those of the mongoose schemas

### /middleware
Haven't really gotten too far with this yet... Contains functions that could be used as middleware in the express server

### /models
Contains the schemas/models used by mongoose. They are exported and then imported into the graphql schema to use in the resolver functions

### /routes
This is just here as an example of what a typical REST api would look like. It isn't actually used anywhere in the app

## TODO
- Create something like a projects schema so that each user could have multiple projects
- Create a front end? Maybe? Might be beyond the scope of this project...
