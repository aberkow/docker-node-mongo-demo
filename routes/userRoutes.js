/*

// Keeping this as an example of what a more typical REST api would look like.

const express = require('express');
const router = express.Router();
const UserModel = require('../models/users');
const middlewareTest = require('../middleware/middleware');

// the '/'  actually refers to http(s)://host/api

router.get('/', middlewareTest.middlewareTest, (req, res) => {
  res.json({ message: 'welcome to the api!' });
});

router.route('/users')
  .post((req, res) => {
    const user = new UserModel();
    user.name = req.body.name;
    user.age = req.body.age;
    user.occupation = req.body.occupation;

    user.save((err) => {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'User created!' });
    })
  })
  .get((req, res) => {
    UserModel.find((err, users) => {
      if (err) {
        res.send(err)
      }
      res.json(users);
    })
  })

router.route('/users/:_id')
  .get((req, res) => {
    UserModel.findById(req.params._id, (err, user) => {
      if (err) {
        res.send(err);
      }
      res.json(user);
    })
  })
  .put((req, res) => {
    UserModel.findById(req.params._id, (err, user) => {
      if (err) {
        res.send(err);
      }
      // set a new name
      user.name = req.body.name;
      user.age = req.body.age;

      // save the data
      user.save((err) => {
        if (err) {
          res.send(err);
        }
        res.json({ message: 'User updated' });
      });
    });
  })
  .delete((req, res) => {
    UserModel.remove({
      _id: req.params._id
    }, (err, user) => {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'User deleted' });
    });
  });

// export the router for users.
module.exports = router;

*/